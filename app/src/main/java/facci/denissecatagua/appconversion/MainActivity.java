package facci.denissecatagua.appconversion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView lbl_nombre, lbl_resultado;
    EditText txt_grados;
    Button btn_convertir;
    public Switch swt_cambiar;
    Boolean bandera= false;
    double resul;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lbl_nombre = (TextView) findViewById(R.id.lbl_nombre);
        txt_grados = (EditText) findViewById(R.id.txt_grados);
        btn_convertir = (Button) findViewById(R.id.btn_convertir);
        swt_cambiar = (Switch) findViewById(R.id.switch_);
        lbl_resultado= (TextView) findViewById(R.id.lbl_resultado);

        btn_convertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String grado = "";
                resul = Double.parseDouble(txt_grados.getText().toString());
                if (swt_cambiar.isChecked()){
                    resul = (resul - 32)/1.8;
                    lbl_resultado.setText(Double.toString(resul) + " Celcius");
                }else{
                    resul = (resul * 1.8)+32;
                    lbl_resultado.setText(Double.toString(resul) + " Farenheit");

                }
            }
        });

        swt_cambiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (swt_cambiar.isChecked()){
                    swt_cambiar.setText("Fahrenheit a Celcius");
                    lbl_nombre.setText("Fahrenheit");
                    bandera = true;
                }else{
                    swt_cambiar.setText("Celcius a Fahrenheit");
                    lbl_nombre.setText("Celcius");
                    bandera = false;
                }

            }
        });
        Log.e("Aplicaiones Moviles I", "Denisse Belen Catagua Valencia");
    }

}